﻿using test.Cache;
using test.ISerivces;

namespace test
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<ICacheData, CacheData>();

            services.Scan(scan => scan
           .FromAssemblyOf<IUserService>()
                .AddClasses(classes => classes.Where(type => type.Name.EndsWith("Service")))
                   .AsImplementedInterfaces()
                   .WithScopedLifetime());
            return services;
        }
    }
}
