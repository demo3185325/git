﻿using test.Request;
using test1.Data.Models;

namespace test.ISerivces
{
    public interface IUserService
    {
        List<User> GetAll();

        User GetById(Guid id);

        bool InsertOrUpdate(User data);

        bool Delete(Guid id);

        void RemoveCache();
    }
}
