﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using test.ISerivces;
using test.Request;
using test1.Data.Models;

namespace test.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {

        List<User> user = new List<User>();

        private readonly ILogger<UserController> _logger;
        private readonly IOptions<ConnectionStrings> _connectionStrings;
        private readonly IUserService _userService;
        public UserController(ILogger<UserController> logger, IOptions<ConnectionStrings> connectionStrings, IUserService userService)
        {
            _logger = logger;
            _connectionStrings = connectionStrings;
            _userService = userService;
        }

        //GETALL
        [HttpGet(Name = "GetAll")]
        public DataApiCollection<User> GetAll()
        {
            var result = new DataApiCollection<User>();
            try
            {
                var users = new List<User>();

                var list = _userService.GetAll();
                if (list.Count > 0)
                {
                    users = list;
                    result.Message = "Success";
                }
                else
                {
                    result.Message = "No Data";
                }
                result.Data = users;
                result.Success = true;
                result.Status = 200;
                return result;
            }
            catch (Exception)
            {
                result.Success = false;
                result.Status = 400;
                result.Message = "Exception0";
                return result;
            }

        }


        //GETBYID
        [HttpPost, Route("GetbyId")]
        public DataApiCollection<User> GetbyId([FromBody] IdRequest data)
        {
            var result = new DataApiCollection<User>();
            try
            {
                var products = new List<User>();

                var obj = _userService.GetById(data.Id);
                if (obj != null)
                {
                    //products.Add(obj);
                    products = new List<User> { obj };
                    result.Message = "Success";
                }
                else
                {
                    result.Message = "No Data";
                }
                result.Data = products;
                result.Success = true;
                result.Status = 200;
                return result;
            }
            catch (Exception)
            {
                result.Success = false;
                result.Status = 400;
                result.Message = "Exception0";
                return result;
            }
        }

        //ADD
        [HttpPost, Route("InsertOrUpdate")]
        public DataApiCollection<User> Post([FromBody] User data)
        {
            var result = new DataApiCollection<User>();
            var isAdd = _userService.InsertOrUpdate(data);
            if (isAdd == true)
            {
                result.Success = isAdd;
                result.Status = 200;
                result.Message = "Đăng kí thành công";
            }
            else
            {
                result.Status = 400;
                result.Message = "Account đã tồn tại";
            }
            return result;
        }

        //DELETE
        [HttpDelete, Route("Delete")]
        public DataApiCollection<User> Delete(Guid id)
        {
            var result = new DataApiCollection<User>();
            try
            {
                var del = _userService.Delete(id);
                result.Success = del;
                result.Status = 200;
                result.Message = "Success";
                return result;
            }
            catch (Exception)
            {
                result.Success = false;
                result.Status = 400;
                result.Message = "Exception4";
                return result;
            }

        }


        public class DataApiCollection<T>
        {
            public List<T> Data { get; set; }

            public bool Success { get; set; }

            public short Status { get; set; }

            public string Message { get; set; }
        }
    }
}
