﻿namespace test.Request
{
    public class IdRequest
    {
        public Guid Id { get; set; } = Guid.NewGuid();
    }
}
