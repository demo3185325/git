﻿namespace test.Request
{
    public class RegisterRq
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int Admin { get; set; }

    }
}
