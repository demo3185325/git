using Autofac.Extensions.DependencyInjection;
using test;
using test1.Data;

public class Program
{
    public static void Main(string[] args)
    {
        ConfigurationHelper.Configuration();
        CreateHostBuilder(args).Build().Run();
    }


    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            }).UseServiceProviderFactory(new AutofacServiceProviderFactory());
}
