﻿using test1.Data.Models;
using test1.Data.Procedure;

namespace test1.CallService
{
    public class UserCallSerivce
    {
        private static UserProcedure db = new UserProcedure();

        #region[GetAllUser]
        public static List<User> GetAllUser()
        {
            return db.GetAllUser();
        }
        #endregion

        #region[GetUserbyID]
        public static List<User> GetUserbyID(int Id)
        {
            return db.GetUserbyID(Id);
        }
        #endregion

        #region[AddUser]
        public static bool AddUser(User data)
        {
            return db.AddUser(data);
        }
        #endregion


        #region[UpdateUser]
        public static bool UpdateUser(User data)
        {
            return db.UpdateUser(data);
        }
        #endregion


        #region[DeleteUser]
        public static bool DeleteUser(Guid Id)
        {
            return db.DeleteUser(Id);
        }
        #endregion
    }
}