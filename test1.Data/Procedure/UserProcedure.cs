﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test1.Data.Models;

namespace test1.Data.Procedure
{
    public class UserProcedure : SqlDataProvider
    {
        #region[GetUserbyID]
        public List<User> GetUserbyID(int Id)
        {
            List<User> list = new List<User>();
            using (SqlCommand dbCmd = new SqlCommand("GetUserbyId", GetConnection()))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@Id", Id));
                SqlDataReader dr = dbCmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        User obj = new User();
                        obj.Id = (Guid)(dr["Id"] is DBNull ? Guid.Empty : dr["Id"]);
                        obj.Name = dr["Name"] is DBNull ? string.Empty : dr["Name"].ToString();
                        obj.Phone = dr["Phone"] is DBNull ? string.Empty : dr["Phone"].ToString();
                        obj.Address = dr["Address"] is DBNull ? string.Empty : dr["Address"].ToString();
                        obj.Admin = dr["Admin"] is DBNull ? 0 : int.Parse(dr["Admin"].ToString());
                        list.Add(obj);
                    }
                }
                dr.Close();
                dr.Dispose();
            }
            return list;
        }
        #endregion

        #region[GetAllUser]
        public List<User> GetAllUser()
        {
            List<User> list = new List<User>();
            using (SqlCommand dbCmd = new SqlCommand("GetAllUser", GetConnection()))
            {

                dbCmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = dbCmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        User obj = new User();
                        obj.Id = (Guid)(dr["Id"] is DBNull ? Guid.Empty : dr["Id"]);
                        obj.Name = dr["Name"] is DBNull ? string.Empty : dr["Name"].ToString();
                        obj.Phone = dr["Phone"] is DBNull ? string.Empty : dr["Phone"].ToString();
                        obj.Address = dr["Address"] is DBNull ? string.Empty : dr["Address"].ToString();
                        obj.Admin = dr["Admin"] is DBNull ? 0 : int.Parse(dr["Admin"].ToString());
                        list.Add(obj);
                    }
                }
                dr.Close();
                dr.Dispose();
            }
            return list;
        }
        #endregion

        #region[AddUser]
        public bool AddUser(User data)
        {
            using (SqlCommand dbCmd = new SqlCommand("InsertUser", GetConnection()))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@Id", data.Id));
                dbCmd.Parameters.Add(new SqlParameter("@Name", data.Name));
                dbCmd.Parameters.Add(new SqlParameter("@Phone", data.Phone));
                dbCmd.Parameters.Add(new SqlParameter("@Address", data.Address));
                dbCmd.Parameters.Add(new SqlParameter("@Admin", data.Admin));
                dbCmd.ExecuteNonQuery();
            }
            return true;
        }
        #endregion

        #region[UpdateUser]
        public bool UpdateUser(User data)
        {
            using (SqlCommand dbCmd = new SqlCommand("UpdatetUser", GetConnection()))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@Id", data.Id));
                dbCmd.Parameters.Add(new SqlParameter("@Name", data.Name));
                dbCmd.Parameters.Add(new SqlParameter("@Phone", data.Phone));
                dbCmd.Parameters.Add(new SqlParameter("@Address", data.Address));
                dbCmd.Parameters.Add(new SqlParameter("@Admin", data.Admin));
                dbCmd.ExecuteNonQuery();
            }
            return true;
        }
        #endregion

        #region[DeleteUser]
        public bool DeleteUser(Guid Id)
        {
            using (SqlCommand dbCmd = new SqlCommand("DeltUser", GetConnection()))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@Id", Id));
                dbCmd.ExecuteNonQuery();
            }
            return true;
        }
        #endregion
    }
}
