﻿using Microsoft.Extensions.Configuration;

namespace test1.Data
{
    public class ConfigurationHelper
    {
        private static IConfiguration config;
        public static IConfiguration Configuration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                //.AddJsonFile($"appsettings.{ Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", true)
                .AddEnvironmentVariables();
            config = builder.Build();
            return config;
        }

        public static T GetConfiguration<T>(string SectionName) where T : class
        {
            if (typeof(T).Name == "String")
            {
                return (T)Convert.ChangeType(config.GetSection(SectionName).Value, typeof(T));
            }
            else
            {
                var section = (T)Activator.CreateInstance(typeof(T));
                config.GetSection(SectionName).Bind(section);
                return section;
            }
        }
    }
}
