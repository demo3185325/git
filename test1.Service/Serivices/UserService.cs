﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test1.Data.Models;
using test1.Service.Cache;
using test1.Service.IServices;

namespace test1.Service.Serivices
{
    public class UserService : IUserService
    {
        private readonly ICacheData _cacheData;
        public UserService(ICacheData cacheData)
        {
            _cacheData = cacheData;
        }

        //GETALL
        public List<User> GetAll()
        {
            var data = new List<User>();

            string cache = "ListUser";

            var cacheData = _cacheData.Get<List<User>>(cache);

            if (cacheData != null && cacheData.Count > 0)
            {
                data = cacheData;
            }
            else
            {
                var list = test1.CallService.UserCallSerivce.GetAllUser();
                if (list != null && list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        User obj = new User();
                        obj.Id = list[i].Id;
                        obj.Name = list[i].Name;
                        obj.Phone = list[i].Phone;
                        obj.Address = list[i].Address;
                        obj.Admin = list[i].Admin;
                        data.Add(obj);
                    }
                }
                if (data != null && data.Count > 0)
                {
                    _cacheData.Set(cache, data, CacheTime.Default);
                }
            }
            return data;
        }

        //GETUSERBYID
        public User GetById(Guid id)
        {
            return GetAll()?.FirstOrDefault(x => x.Id == id);
        }


        //ADD
        public bool InsertOrUpdate(User data)
        {
            var guid = Guid.NewGuid();
            var result = true;
            var check = GetAll()?.FirstOrDefault(x => x.Id == data.Id);
            if (check == null)
            {
                User obj = new User();
                obj.Id = guid;
                obj.Name = data.Name;
                obj.Phone = data.Phone;
                obj.Address = data.Address;
                obj.Admin = data.Admin;
                var add = test1.CallService.UserCallSerivce.AddUser(obj);
                if (add == true)
                {
                    // XÓA CACHE CŨ ĐI VÀ GỌI LẠI FULL CACHE MỚI
                    RemoveCache();
                    GetAll();
                }
            }
            else
            {
                User obj = new User();
                obj.Id = check.Id;
                obj.Name = data.Name;
                obj.Phone = data.Phone;
                obj.Address = data.Address;
                obj.Admin = data.Admin;
                var update = test1.CallService.UserCallSerivce.UpdateUser(obj);
                if (update == true)
                {
                    RemoveCache();
                    GetAll();
                }
            }
            return result;
        }

        //DeleteUser
        public bool Delete(Guid id)
        {
            try
            {
                var user = GetAll()?.FirstOrDefault(x => x.Id == id);
                if (user != null)
                {
                    test1.CallService.UserCallSerivce.DeleteUser(id);
                    RemoveCache();
                    GetAll();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //RemoveCache
        public void RemoveCache()
        {
            //Xóa Cache
            _cacheData.Remove("ListUser");
        }
    }
}
