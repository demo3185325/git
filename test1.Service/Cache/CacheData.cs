﻿using Microsoft.Extensions.Caching.Memory;

namespace test1.Service.Cache
{
    public class CacheData : ICacheData
    {
        private readonly IMemoryCache _memoryCache;

        public CacheData(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public void Set<T>(string key, T obj, TimeSpan time)
        {
            _memoryCache.Set(key, obj, time);
        }

        public T Get<T>(string key)
        {
            return _memoryCache.Get<T>(key);
        }

        public void Remove(string key)
        {
            _memoryCache.Remove(key);
        }
    }
    public class CacheTime
    {
        public static TimeSpan Default = TimeSpan.FromDays(1);

        public static TimeSpan Default20minutes = TimeSpan.FromMinutes(20);
    }
}
