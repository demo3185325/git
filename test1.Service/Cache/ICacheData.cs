﻿namespace test1.Service.Cache
{
    public interface ICacheData
    {
        void Set<T>(string key, T obj, TimeSpan time);

        T Get<T>(string key);

        void Remove(string key);
    }
}
