﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test1.Data.Models;

namespace test1.Service.IServices
{
    public interface IUserService
    {
        List<User> GetAll();

        User GetById(Guid id);

        bool InsertOrUpdate(User data);

        bool Delete(Guid id);

        void RemoveCache();
    }
}
